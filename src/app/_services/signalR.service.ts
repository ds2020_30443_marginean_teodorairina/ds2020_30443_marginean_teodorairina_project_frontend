import { Injectable } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import { Subject, Observable } from 'rxjs';
import { AlertifyService } from './alertify.service';

@Injectable({
  providedIn: 'root'
})
export class SignalRService {

  private signalRHubConnection: signalR.HubConnection;
  private notification: Subject<string>;
  private patientEmail: string;

  constructor(private aleritfy: AlertifyService) { }

   public startListening() {
      this.signalRHubConnection = new signalR.HubConnectionBuilder()
      .withUrl('https://localhost:5001/hub')
      .build();

      this.signalRHubConnection
          .start()
          .then(() => console.log('Connected'))
          .catch((error) => console.log(error));
    }

    public getNotification() {
      this.signalRHubConnection.on('notification/' + this.patientEmail, (activity) => {
        this.aleritfy.message(activity);
      });
    }
}
